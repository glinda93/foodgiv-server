# frozen_string_literal: true

require 'down'

food_attributes = [
  {
    title: 'Striploin Steak',
    description: 'Striploin Steak',
    food_type: 'UNPREPARED',
    quantity: 3,
    pickup_time: '2:00 PM',
    exp_date: 3.days.from_now,
    status: 'LISTED',
    location: {
      place_id: 'ChIJOwg_06VPwokRYv534QaPC8g',
      lat: 40.7128,
      lng: -74.0060,
      formatted_address: 'New York, NY, USA'
    },
    images: [
      'https://s3.amazonaws.com/pictures.duocun.ca/media/8d06b08b-b11d-4dc9-88ab-8a65a3589b97.jpg'
    ],
    user: {
      email: 'bravemaster619@hotmail.com',
      first_name: 'brave',
      last_name: 'master',
      phone: '1234567890',
      phone_verified: true,
      profile_image: 'https://en.gravatar.com/userimage/183838592/43066144adcecf74471c53e484ea29ac.png',
      password: '11111111'
    },
    labels: ['Packaged Food', 'Fresh Produce'],
    allergens: [''],
    created_at: DateTime.now,
    listed_at: DateTime.now
  },
  {
    title: 'Dumpling',
    description: 'Chinese dumpling',
    food_type: 'PREPARED',
    quantity: 1,
    pickup_time: 'Anytime between 9:00 AM and 5:00 PM',
    exp_date: 2.days.from_now,
    status: 'LISTED',
    location: {
      place_id: 'ChIJCSF8lBZEwokRhngABHRcdoI',
      lat: 40.6782,
      lng: -73.9442,
      formatted_address: 'Brooklyn, NY, USA'
    },
    images: [
      'https://s3.amazonaws.com/pictures.duocun.ca/media/78cc9513-81fe-4166-88f0-d1603125be4b.jpeg',
      'https://s3.amazonaws.com/pictures.duocun.ca/media/cb7b5005-50e4-44f0-8992-27aa9df6424f.jpg',
      'https://s3.amazonaws.com/pictures.duocun.ca/media/ef20277d-52a4-443d-a6a0-530f7cee0921.jpg'
    ],
    user: {
      email: 'sanasieo234@gmail.com',
      first_name: 'Sana',
      last_name: 'Minatozaki',
      phone: '9876543210',
      phone_verified: true,
      profile_image: 'https://d1ydle56j7f53e.cloudfront.net/assets/gallery/profile/sana-khan-10012/misc-big/sana-khan-580.jpg',
      password: '11111111'
    },
    labels: %w[Organic Gluten-Free],
    allergens: %w[Wheat Fish],
    created_at: DateTime.now,
    listed_at: DateTime.now
  },
  {
    title: 'Shrimp',
    description: 'Frozen shrimp',
    food_type: 'UNPREPARED',
    quantity: 4,
    pickup_time: '12:00 PM',
    exp_date: 7.days.from_now,
    status: 'LISTED',
    location: {
      place_id: 'ChIJpTvG15DL1IkRd8S0KlBVNTI',
      lat: 43.6532,
      lng: -79.3832,
      formatted_address: 'Toronto, ON, Canada'
    },
    images: [
      'https://s3.amazonaws.com/pictures.duocun.ca/media/498eb3fc-d684-4ec0-b15a-e06f1bbf9380.jpg'
    ],
    user: {
      email: 'bravemaster619@hotmail.com',
      first_name: 'brave',
      last_name: 'master',
      phone: '1234567890',
      phone_verified: true,
      profile_image: 'https://d1ydle56j7f53e.cloudfront.net/assets/gallery/profile/sana-khan-10012/misc-big/sana-khan-580.jpg',
      password: '11111111'
    },
    labels: %w[Raw Organic Gluten-Free Oil-Free],
    allergens: %w[Fish Shellfish],
    created_at: 2.days.ago,
    listed_at: 2.days.ago
  }
]

def get_user(attr)
  return User.find user_id if attr[:user_id]

  user_attr = attr[:user]
  user = User.where(email: user_attr[:email]).first
  unless user
    user = User.create!(
      email: user_attr[:email],
      first_name: user_attr[:first_name],
      last_name: user_attr[:last_name],
      phone: user_attr[:phone],
      phone_verified: user_attr[:phone_verified],
      password: user_attr[:password]
    )
    if user_attr[:profile_image].present?
      user.profile_image = Image.new(image: Down.open(user_attr[:profile_image]))
      user.save!
    end
  end
  user
end

def get_location(attr)
  return Location.find location_id if attr[:location_id]

  location_attr = attr[:location]
  location = Location.where(place_id: location_attr[:place_id]).first
  location ||= Location.create(location_attr)
  location
end

def get_images(attr)
  images = []
  attr[:images].each do |url|
    image = Image.new(image: Down.open(url))
    image.image_derivatives!
    images << image
  end
  images
end

def get_label(label_name)
  label = Label.where(name: label_name).first
  label ||= Label.create!(name: label_name)
  label
end

def get_allergen(allergen_name)
  allergen = Allergen.where(name: allergen_name).first
  allergen ||= Allergen.create!(name: allergen_name)
  allergen
end

def create_label_relations(food, labels)
  label_ids = []
  labels.reject(&:empty?).each do |label_name|
    label = get_label label_name
    label_ids << label[:id]
  end
  food.label_ids = label_ids
  food.save!
end

def create_allergen_relations(food, allergens)
  allergen_ids = []
  allergens.reject(&:empty?).each do |allergen_name|
    allergen = get_allergen allergen_name
    allergen_ids << allergen[:id]
  end
  food.allergen_ids = allergen_ids
  food.save!
end

count = 0

food_attributes.each do |attr|
  food = Food.where(title: attr[:title]).first
  next if food

  food_attr = {
    title: attr[:title],
    description: attr[:description],
    food_type: attr[:food_type].to_s,
    quantity: attr[:quantity],
    pickup_time: attr[:pickup_time],
    exp_date: attr[:exp_date],
    status: attr[:status],
    user_id: get_user(attr)[:id],
    location_id: get_location(attr)[:id],
    images: get_images(attr)
  }
  food = Food.new food_attr
  puts food.images.inspect unless food.save
  create_label_relations(food, attr[:labels]) if attr[:labels].present?
  create_allergen_relations(food, attr[:allergens]) if attr[:allergens].present?
  count += 1
end

puts "Seeded #{count} foods."

# frozen_string_literal: true

class AddListedAtToFoods < ActiveRecord::Migration[5.2]
  def change
    add_column :foods, :listed_at, :datetime, null: true
  end
end

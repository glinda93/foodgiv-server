# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :first_name, null: false, default: ''
      t.string :last_name
      t.string :email # can be null if sign up with facebook
      t.string :phone
      t.string :password_digest
      t.string :verification_code
      t.boolean :phone_verified, default: false
      t.text :profile_image
      t.text :summary
      t.text :favorite_food
      t.text :hated_food
      t.string :current_sign_in_ip
      t.string :last_sign_in_ip
      t.datetime :current_sign_in_at
      t.string :stripe_customer_id
      t.string :stripe_subscription_id
      t.string :google_user_id
      t.string :facebook_user_id
      t.integer :giver_review_count, default: 0
      t.float :giver_rating, default: 0
      t.integer :receiver_review_count, default: 0
      t.float :receiver_rating, default: 0
      t.boolean :notification_allowed, default: false
      t.string :device_token
      t.string :notification_frequency
      t.integer :notification_distance, default: 0
      t.string :status, default: 'ACTIVE'
      t.datetime :created_at, null: false
    end
  end
end

# frozen_string_literal: true

require 'down'
require 'google-id-token'
require "net/http"

module SocialLoginHelper
  def find_or_create_by_social_auth(credentials)
    case credentials[:provider]
    when 'GOOGLE'
      find_or_create_google_user credentials
    when 'FACEBOOK'
      find_or_create_facebook_user credentials
    end
  end

  def find_or_create_google_user(credentials)
    payload = get_google_user(credentials[:access_token])
    # find by google user id
    google_user_id = payload['sub']
    user = User.where(google_user_id: google_user_id).first
    return user if user.present?
    # find by gmail
    user = User.where(email: payload['email']).first
    if user.present?
      user.google_user_id = google_user_id
      user.save!
      return user
    end
    # create user
    user = User.create({
                         first_name: payload['given_name'],
                         last_name: payload['family_name'],
                         email: payload['email'],
                         google_user_id: google_user_id,
                         password: generate_random_password
                       })
    # download profile image
    begin
      user.profile_image = fetch_profile_image(payload['picture'])
    rescue StandardError => e
      Rails.logger.error 'Cannot save profile image for this payload from google'
      Rails.logger.info payload.inspect
      Rails.logger.error e.message
    end
    user.save!(validate: false)
    user
  end

  def find_or_create_facebook_user(credentials)
    payload = get_facebook_user(credentials[:access_token])
    return nil if payload.nil?
    # find by facebook user id
    facebook_user_id = payload['id']
    user = User.where(facebook_user_id: facebook_user_id).first
    return user if user.present?
    # find by facebook email
    if payload['email'].present?
      user = User.where(email: payload['email']).first
      if user.present?
        user.facebook_user_id = facebook_user_id
        user.save!(validate: false)
        return user
      end
    end
    # create user
    user = User.create({
                        first_name: payload['first_name'],
                        last_name: payload['last_name'],
                        email: payload['email'],
                        facebook_user_id: facebook_user_id,
                        password: generate_random_password
                      })
    # download profile image
    begin
      user.profile_image = fetch_profile_image(payload['picture']['data']['url'])
    rescue => exception
      Rails.logger.error 'Cannot save profile image for this payload from facebook'
      Rails.logger.info payload.inspect
      Rails.logger.error e.message
    end
    user.save!(validate: false)
    user
  end

  def fetch_profile_image(url)
    pic = Image.new
    pic.image = Down.open url
    pic.image_derivatives!
    pic
  end

  def get_google_user(access_token)
    validator = GoogleIDToken::Validator.new
    validator.check(
      access_token,
      Rails.configuration.google.oauth_web_client_id
    )
  rescue StandardError => e
    Rails.logger.error(e.message)
    nil
  end

  def get_facebook_user(access_token)
    url = "https://graph.facebook.com/me?fields=id,first_name,last_name,email,picture&access_token=#{access_token}"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.open_timeout = 5
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Get.new(uri.to_s)
    response = http.request request
    response = JSON.parse response.body
    if response['id'].present?
      response
    else
      Rails.logger.warn "Facebook response did not return user id"
      Rails.logger.info response
      nil
    end
  end

  def generate_random_password(len: 32)
    ('0'..'z').to_a.shuffle.first(len).join
  end
end

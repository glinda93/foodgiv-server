# frozen_string_literal: true

class Request < ApplicationRecord
  belongs_to :giver, class_name: 'User'
  belongs_to :receiver, class_name: 'User'
  belongs_to :food

  has_many :reviews, -> { order(created_at: :desc, id: :desc) }
  has_one :review_give, -> { where(review_type: 'FOR_GIVER') }, class_name: 'Review'
  has_one :review_receive, -> { where(review_type: 'FOR_RECEIVER') }, class_name: 'Review'
  has_many :messages, -> { order(created_at: :desc, id: :desc) }

  default_scope { where.not(status: 'DELETED') }
  scope :deleted, -> { unscope(where: :status).where(status: 'DELETED') }
  scope :where_belongs_to, ->(user) { where(giver_id: user.id).or(where(receiver_id: user.id)) }
  scope :archived, ->(user) { where(giver_id: user.id, giver_archived: true).or(where(receiver_id: user.id, receiver_archived: true)) }
  scope :unarchived, ->(user) { where(giver_id: user.id, giver_archived: false).or(where(receiver_id: user.id, receiver_archived: false)) }
  scope :where_no_review_by, ->(user) { where_belongs_to(user).joins("LEFT OUTER JOIN reviews ON reviews.request_id = requests.id AND reviews.reviewer_id = #{user.id}").where(reviews: { id: nil }) }
  scope :where_review_created_by, ->(user) { where_belongs_to(user).joins("INNER JOIN reviews ON reviews.request_id = requests.id AND reviews.reviewer_id = #{user.id}") }
  scope :where_review_received_by, ->(user) { where_belongs_to(user).joins("INNER JOIN reviews ON reviews.request_id = requests.id AND reviews.reviewer_id != #{user.id}") }
  after_update :notify_to_channels

  attr_reader :last_action_at

  def giver_id?(user_id)
    giver.nil? ? false : giver.id == user_id
  end

  def receiver_id?(user_id)
    receiver.nil? ? false : receiver.id == user_id
  end

  def giver?(user)
    user.nil? ? false : giver_id?(user.id)
  end

  def receiver?(user)
    user.nil? ? false : receiver_id?(user.id)
  end

  def related?(user)
    giver?(user) || receiver?(user)
  end

  def archived?(user)
    if giver?(user)
      giver_archived
    elsif receiver?(user)
      receiver_archived
    end
  end

  def get_relationship(user)
    if giver?(user)
      'GIVER'
    elsif receiver?(user)
      'RECEIVER'
    else
      'UNKNOWN'
    end
  end

  def get_other_party(user)
    case get_relationship(user)
    when 'GIVER'
      receiver
    when 'RECEIVER'
      giver
    end
  end

  def last_message
    messages.first
  end

  def last_action_at
    if messages.first.nil?
      last_status_changed_at
    else
      [messages.first.created_at, last_status_changed_at].max
    end
  end

  def last_status_changed_at
    [created_at, declined_at, accepted_at, completed_at, cancelled_at, last_review_at].select(&:present?).max
  end

  def last_review_at
    review_give_at = review_give.nil? ? nil : review_give.created_at
    review_receive_at = review_receive.nil? ? nil : review_receive.created_at
    [review_give_at, review_receive_at].select(&:present?).max
  end

  def unread_message_count(user)
    messages.unread(user).count
  end

  def giver_sent_message?
    Message.where(request_id: id, food_id: food.id, sender_id: giver.id).count > 0
  end

  private

  def notify_to_channels
    # Request Channel
    BroadcastJob.perform_later room: "requests:#{id}", data: self, event: 'RequestUpdated'
    # Appearance Channel
    [giver, receiver].each do |user|
      BroadcastJob.perform_later room: "users:#{user.id}", event: 'InboxUpdated'
    end
  end
end

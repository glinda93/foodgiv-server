# frozen_string_literal: true

class Location < ApplicationRecord
  acts_as_mappable

  after_update :clear_distance_calculations

  validates :lat, presence: true
  validates :lng, presence: true
  validates :formatted_address, presence: true, length: { maximum: 191 }

  private

  def clear_distance_calculations
    Rails.cache.delete_matched("distance:#{id}-*")
    Rails.cache.delete_matched("distance:*-#{id}")
  end
end

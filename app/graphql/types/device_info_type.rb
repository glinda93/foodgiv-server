# frozen_string_literal: true

module Types
  class DeviceTypeEnum < Types::BaseEnum
    value 'ANDROID'
    value 'IOS'
  end

  class DeviceInfoType < Types::BaseObject
    field :device_token, String, null: false
    field :device_type, Types::DeviceTypeEnum, null: false
  end
end

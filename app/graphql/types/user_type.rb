# frozen_string_literal: true

module Types
  class UserStatusEnum < Types::BaseEnum
    value 'DELETED'
    value 'DISABLED'
    value 'ACTIVE'
  end

  class UserPublicAttributeType < Types::BaseObject
    field :id, ID, null: false
    field :first_name, String, null: false
    field :last_name, String, null: true
    field :profile_image, ImageType, null: true
    field :phone_verified, Boolean, null: false
    field :summary, String, null: true
    field :favorite_food, String, null: true
    field :hated_food, String, null: true
    field :location, LocationType, null: true
    field :giver_review_count, Integer, null: true
    field :giver_rating, Float, null: true
    field :receiver_review_count, Integer, null: true
    field :receiver_rating, Float, null: true
    field :created_at, String, null: false

    def profile_image
      RecordLoader.for(Image).load(object.image_id)
    end

    def location
      RecordLoader.for(Location).load(object.location_id)
    end
  end

  class UserType < UserPublicAttributeType
    field :email, String, null: true
    field :phone, String, null: true
    field :phone_verified, Boolean, null: false
    field :current_sign_in_ip, String, null: true
    field :last_sign_in_ip, String, null: true
    field :current_sign_in_at, String, null: false
    field :stripe_customer_id, String, null: true
    field :stripe_subscription_id, String, null: true
    field :google_user_id, String, null: true
    field :facebook_user_id, String, null: true
    field :notification_allowed, Boolean, null: true
    field :device_info, Types::DeviceInfoType, null: true
    field :notification_frequency, String, null: true
    field :notification_distance, Integer, null: true
    field :status, UserStatusEnum, null: false
  end
end

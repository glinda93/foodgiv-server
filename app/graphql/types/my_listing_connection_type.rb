# frozen_string_literal: true

module Types
  class MyListingConnectionFilterStatusEnum < Types::BaseEnum
    value 'LISTED'
    value 'UNLISTED'
    value 'ACCEPTED'
    value 'ARCHIVED'
    value 'REQUESTED'
  end

  class MyListingConnectionFilterType < Types::BaseInputObject
    argument :status, Types::MyListingConnectionFilterStatusEnum, required: false
  end

  class MyListingEdgeType < GraphQL::Types::Relay::BaseEdge
    node_type(Types::MyListingType)
  end

  class MyListingConnectionType < GraphQL::Types::Relay::BaseConnection
    field :total_count, Integer, null: false
    def total_count
      object.nodes.size
    end
    edge_type(MyListingEdgeType)
  end
end

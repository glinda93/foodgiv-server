# frozen_string_literal: true

module Types
  class MutationType < Types::BaseObject
    # authentication
    field :create_user, mutation: Mutations::CreateUser
    field :signin_user, mutation: Mutations::SignInUser
    field :send_verification_code, mutation: Mutations::SendVerificationCode
    field :send_otp, mutation: Mutations::SendOtp
    field :verify_phone, mutation: Mutations::VerifyPhone
    # profile, user setting
    field :save_profile, mutation: Mutations::SaveProfile
    field :save_user_setting, mutation: Mutations::SaveUserSetting
    field :delete_account, mutation: Mutations::DeleteAccount
    # food
    field :save_food, mutation: Mutations::SaveFood
    # request
    field :send_request, mutation: Mutations::SendRequest
    field :accept_request, mutation: Mutations::AcceptRequest
    field :decline_request, mutation: Mutations::DeclineRequest
    field :cancel_request, mutation: Mutations::CancelRequest
    field :complete_request, mutation: Mutations::CompleteRequest
    field :archive_request, mutation: Mutations::ArchiveRequest
    # message
    field :send_message, mutation: Mutations::SendMessage
    field :archive_message, mutation: Mutations::ArchiveMessage
    # feedback
    field :save_review, mutation: Mutations::SaveReview
    # payment
    field :create_subscription, mutation: Mutations::CreateSubscription
    if Rails.env.development? || Rails.env.test?
      field :push_notification_test, mutation: Mutations::PushNotificationTest
    end
  end
end

# frozen_string_literal: true

module Types
  class ErrorType < Types::BaseObject
    description 'Error response type'
    field :message, String, null: false
    field :path, [String], null: true
  end
end

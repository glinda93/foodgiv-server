# frozen_string_literal: true

module Types
  class ReviewType < Types::BaseObject
    field :id, ID, null: false
    field :request, Types::RequestType, null: true
    field :food, Types::FoodType, null: true
    field :giver, Types::UserPublicAttributeType, null: true
    field :receiver, Types::UserPublicAttributeType, null: true
    field :reviewer, Types::UserPublicAttributeType, null: true
    field :review_type, String, null: true
    field :food_quality, Float, null: true
    field :pickup_exp, Float, null: true
    field :collector_exchange_exp, Float, null: true
    field :feedback, String, null: true
    field :rating_overall, Float, null: false
    field :created_at, String, null: false
    field :updated_at, String, null: true

    def request
      RecordLoader.for(Request).load(object.request_id)
    end

    def food
      RecordLoader.for(Food).load(object.food_id)
    end

    def giver
      RecordLoader.for(User).load(object.giver_id)
    end

    def receiver
      RecordLoader.for(User).load(object.receiver_id)
    end

    def reviewer
      RecordLoader.for(User).load(object.reviewer_id)
    end
  end
end

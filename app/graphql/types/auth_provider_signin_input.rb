# frozen_string_literal: true

module Types
  class AuthProviderSigninInput < Types::BaseInputObject
    graphql_name 'AUTH_PROVIDER_SIGN_IN_INPUT'

    argument :provider, AuthProviderEnum, required: true
    argument :email, String, required: false
    argument :access_token, String, required: false
    argument :password, String, required: false
    argument :otp, String, required: false
  end

  class SocialAuthInput < Types::BaseInputObject
    argument :provider, SocialAuthProviderEnum, required: true
    argument :access_token, String, required: true
  end
end

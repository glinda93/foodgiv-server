# frozen_string_literal: true

module MyListingConnection
  extend ActiveSupport::Concern

  included do
    field :my_listing_connection, Types::MyListingConnectionType, null: false, max_page_size: 10 do
      description 'Get my listing food connection'
      argument :filter, Types::MyListingConnectionFilterType, required: false
    end
  end

  def my_listing_connection(input = { filter: nil })
    auth_check
    user = context[:current_user]
    scope = Food
    if input[:filter] && input[:filter][:status]
      scope = if input[:filter][:status] == 'ARCHIVED'
                scope.where(status: 'COMPLETED').or(Food.expired)
              elsif input[:filter][:status] == 'REQUESTED'
                scope.where_has_requests
              else
                scope.where(status: input[:filter][:status])
              end
    end
    scope = scope.where(user_id: user.id)
    scope = scope.order(created_at: :desc, id: :desc)
    scope
  end
end

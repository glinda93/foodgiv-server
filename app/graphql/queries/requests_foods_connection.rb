# frozen_string_literal: true

module RequestsFoodsConnection
  extend ActiveSupport::Concern

  included do
    field :requests_foods_connection, Types::FoodConnectionType, null: false, max_page_size: 10 do
      description 'Returns my food that has any request, or food that I have sent request to. Used to filter requests connection'
      argument :filter, Types::RequestFoodConnectionFilterType, required: false
    end
  end

  def requests_foods_connection(input = { filter: nil })
    auth_check
    user = context[:current_user]
    scope = Food.includes(:requests)
                .where_has_requests_belongs_to_user(user)
    scope = scope.where(status: input[:filter][:status]) if input && input[:filter]
    scope = scope.uniq
                 .sort_by(&:last_action_at)
                 .reverse
    scope
  end
end

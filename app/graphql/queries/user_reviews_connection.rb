# frozen_string_literal: true

module UserReviewsConnection
  extend ActiveSupport::Concern

  included do
    field :user_reviews_connection, Types::ReviewConnectionType, null: false, max_page_size: 10 do
      argument :filter, Types::ReviewConnectionFilterType, required: true
    end
  end

  def user_reviews_connection(input)
    auth_check
    filter = input[:filter]
    begin
      user = User.find filter[:user_id]
    rescue StandardError => e
      Rails.logger.debug e.message
      raise_gql_error code: 'no_such_user'
    end
    scope = Review.where_received_by user
    case filter[:type]
    when 'FOR_GIVER'
      scope = scope.where(review_type: 'FOR_GIVER')
    when 'FOR_RECEIVER'
      scope = scope.where(review_type: 'FOR_RECEIVER')
    end
    scope = scope.order(created_at: :desc, id: :desc)
    scope
  end
end

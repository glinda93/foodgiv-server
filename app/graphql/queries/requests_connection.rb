# frozen_string_literal: true

module RequestsConnection
  extend ActiveSupport::Concern

  included do
    field :requests_connection, Types::RequestConnectionType, null: false, max_page_size: 10 do
      description 'Get my requests connection'
      argument :filter, Types::RequestConnectionFilterType, required: false
    end
  end

  def requests_connection(input = { filter: nil })
    auth_check
    user = context[:current_user]
    scope = Request.includes(:review_give, :review_receive)
    filter = input[:filter]
    scope = if filter.present? && filter[:show_archived]
              scope.where_belongs_to(user)
            else
              scope.unarchived(user)
            end
    scope = scope.where(food_id: filter[:food_id]) if filter && filter[:food_id]
    scope = scope.all.sort_by(&:last_action_at).reverse
    scope
  end
end

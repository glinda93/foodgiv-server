# frozen_string_literal: true

module Mutations
  class AcceptRequest < Mutations::BaseMutation
    include AuthHelper
    include GraphqlHelper
    include AppearanceHelper
    include RpushHelper

    graphql_name 'accept_request'
    description 'Giver accepts a request'
    argument :request_id, Integer, required: true

    field :request, Types::RequestType, null: false

    def resolve(input)
      @input = input
      auth_check
      check_request
      check_food
      save_request
      save_food_status
      decline_other_requests
      notify_to_receiver
      { request: @request }
    end

    def check_request
      @user = context[:current_user]
      @request = Request.where(id: @input[:request_id], giver_id: @user.id)
                        .first
      raise_gql_error code: 'no_such_request' unless @request
      unless @request.status == 'NEW'
        raise_gql_error code: "request_already_#{@request.status.downcase}", msg: I18n.t('graphql.errors.request_invalid_action', action: 'accept', status: @request.status.downcase)
      end
      raise_gql_error code: 'receiver_not_available' unless @request.receiver&.active?
    end

    def check_food
      @food = @request.food
      raise_gql_error code: 'no_such_food' unless @food
      raise_gql_error code: 'food_not_listed' unless @food.status == 'LISTED'
    end

    def save_request
      @request.accepted_at = DateTime.now
      @request.status = 'ACCEPTED'
      @request.save!
    end

    def save_food_status
      @food.status = 'ACCEPTED'
      @food.save!
    end

    def notify_to_receiver
      return if online? user: @request.receiver

      push_request_event request: @request, user: @request.receiver, event: 'accepted'
    end

    def decline_other_requests
      requests = Request
                 .includes(:food, :giver, :receiver)
                 .where.not(id: @request.id)
                 .where(food_id: @request.food.id)
                 .where(status: 'NEW')
                 .all
      requests.each do |request|
        request.status = 'DECLINED'
        request.declined_at = DateTime.now
        request.save!
        user = request.receiver
        notify_decline_to user if user
      end
    end

    def notify_decline_to(user); end
  end
end

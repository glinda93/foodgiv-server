# frozen_string_literal: true

module Mutations
  class SignInUser < Mutations::BaseMutation
    include AuthHelper
    include SocialLoginHelper
    include GraphqlHelper

    graphql_name 'sign_in_user'

    argument :credentials, Types::AuthProviderSigninInput, required: true

    field :user, Types::UserType, null: true
    field :token, String, null: true
    field :errors, Types::ErrorType, null: true

    def resolve(input)
      @credentials = input[:credentials]
      validate_credentials
      @user = if @credentials[:provider] == 'EMAIL'
                if @credentials[:password].present?
                  find_by_email_password
                elsif @credentials[:otp].present?
                  find_by_email_otp
                end
              else
                begin
                  find_or_create_by_social_auth @credentials
                rescue StandardError => e
                  Rails.logger.error e.message
                  nil
                end
              end
      return { user: nil, token: nil } unless @user.present? && @user.active?

      token = generate_jwt
      save_login_info @user
      { user: @user, token: token }
    end

    def generate_jwt
      JsonWebToken.encode(@user.id)
    end

    def find_by_email_password
      email = @credentials[:email]
      password = @credentials[:password]
      user = User.where_email_icase(email).first
      return nil unless user
      return nil unless user.authenticate password

      user
    end

    def find_by_email_otp
      email = @credentials[:email]
      otp = @credentials[:otp]
      user = User.where_email_icase(email).first
      return nil unless user

      real_otp = get_user_otp user
      if real_otp.present? && real_otp == otp
        clear_user_otp user
        user
      end
    end

    def validate_credentials
      if @credentials[:provider] == 'EMAIL'
        raise_gql_error code: 'email_empty' if @credentials[:email].blank?
        if @credentials[:password].blank? && @credentials[:otp].blank?
          raise_gql_error code: 'password_empty'
        end
      else
        raise_gql_error code: 'access_token_empty' if @credentials[:access_token].blank?
      end
    end

    def get_user_otp(user)
      Rails.cache.read("users:#{user.id}:otp")
    end

    def clear_user_otp(user)
      Rails.cache.delete("users:#{user.id}:otp")
    end
  end
end

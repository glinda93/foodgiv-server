# frozen_string_literal: true

module Mutations
  class CancelRequest < Mutations::BaseMutation
    include AuthHelper
    include GraphqlHelper
    graphql_name 'cancel_request'
    description 'Receiver cancels a request, or a giver cancels an accepted request. For giver, if relist field is true, the food status will be set as LISTED'
    argument :request_id, Integer, required: true
    argument :relist, Boolean, required: false
    field :request, Types::RequestType, null: false

    def resolve(input)
      @input = input
      auth_check
      check_request
      save_request
      save_food_status if @relationship == 'GIVER'
      notify_to_giver
      { request: @request }
    end

    def check_request
      @user = context[:current_user]
      @request = Request.where(id: @input[:request_id]).first
      raise_gql_error code: 'no_such_request' unless @request
      @relationship = @request.get_relationship @user
      case @relationship
      when 'GIVER'
        raise_gql_error code: 'request_not_accepted' unless @request.status == 'ACCEPTED'
      when 'RECEIVER'
        unless @request.status == 'NEW'
          raise_gql_error code: "request_already_#{@request.status.downcase}", msg: I18n.t('graphql.errors.request_invalid_action', action: 'cancel', status: @request.status.downcase)
        end
      else
        raise_gql_error code: 'no_such_request'
      end
    end

    def save_request
      case @relationship
      when 'GIVER'
        @request.food.status = 'LISTED'
        @request.food.save!
        @request.accepted_at = nil
        @request.status = 'NEW'
        @request.save!
      when 'RECEIVER'
        @request.cancelled_at = DateTime.now
        @request.status = 'CANCELLED'
        @request.save!
      end
    end

    def save_food_status
      food = @request.food
      raise_gql_error code: 'no_such_food' unless food
      if @input[:relist]
        raise_gql_error code: 'cannot_list_expired_food' if food.expired?
        food.status = 'LISTED'
        food.save!
      end
    end

    def notify_to_giver
      return unless @request.giver
    end
  end
end

# frozen_string_literal: true

module Mutations
  class ArchiveRequest < Mutations::BaseMutation
    include AuthHelper
    include GraphqlHelper
    graphql_name 'archive_request'
    argument :request_id, Integer, required: true
    field :request, Types::RequestType, null: false

    def resolve(input)
      @input = input
      auth_check
      check_request
      save_request
      { request: @request }
    end

    def check_request
      @user = context[:current_user]
      @request = Request.where_belongs_to(@user).where(id: @input[:request_id]).first
      raise_gql_error code: 'no_such_request' unless @request
    end

    def save_request
      @relation = @request.get_relationship @user
      case @relation
      when 'GIVER'
        @request.giver_archived = true
      when 'RECEIVER'
        @request.receiver_archived = true
      end
      @request.save!
    end
  end
end

# frozen_string_literal: true

module Mutations
  class SaveFood < Mutations::BaseMutation
    include AuthHelper
    include GraphqlHelper

    graphql_name 'save_food'
    description 'Create or update a food listing'
    argument :food, Types::SaveFoodInput, required: true

    field :food, Types::FoodType, null: false

    def resolve(input)
      auth_check
      @user = context[:current_user]
      @food_attr = input[:food]
      find_food
      fill_record
      @food.save!
      { food: @food }
    end

    def find_food
      if @food_attr[:id].present?
        @food = Food.where(id: @food_attr[:id], user_id: @user.id).first
        raise_gql_error code: 'no_such_food' unless @food
      else
        @food = Food.new
      end
      @food
    end

    def fill_record
      raise_gql_error code: 'cannot_edit_completed_listing' if food_finished?
      set_basic_info
      set_status
      set_associations
    end

    def food_finished?
      @food.status == 'COMPLETED' || @food.status == 'FAILED'
    end

    def set_basic_info
      @food.title = @food_attr[:title]
      @food.description = @food_attr[:description]
      @food.food_type = @food_attr[:food_type]
      @food.quantity = @food_attr[:quantity]
      @food.pickup_time = @food_attr[:pickup_time]
      if @food_attr[:exp_date].present?
        begin
          @food.exp_date = DateTime.parse @food_attr[:exp_date]
        rescue StandardError
          raise_gql_error code: 'invalid_expiry_date'
        end
      end
      if @food[:id].blank?
        raise_gql_error code: 'cannot_list_expired_food' if @food.expired?
      end
    end

    def set_status
      return if @food_attr[:status].blank?

      # food_attr status is either LISTED or UNLISTED
      if @food.status.blank? || @food.status == 'LISTED' || @food.status == 'UNLISTED'
        if @food_attr[:status] == 'LISTED' && @food.status != 'LISTED'
          @food.listed_at = DateTime.now
        end
        @food.status = @food_attr[:status]
      else
        # raise_gql_error('You cannot edit status for this food', 'cannot_edit_status')
      end
    end

    def set_associations
      set_location
      set_labels_and_allergens
      set_images
      @food.user = @user
    end

    def set_location
      location_attr = @food_attr[:location]
      location = if location_attr[:id].present?
                   Location.find location_attr[:id]
                 else
                   Location.new
                  end
      raise_gql_error code: 'no_such_location' unless location

      location.place_id = location_attr[:place_id]
      location.lat = location_attr[:lat]
      location.lng = location_attr[:lng]
      location.formatted_address = location_attr[:formatted_address]
      @food.location = location
    end

    def set_labels_and_allergens
      labels = []
      @food_attr[:labels_ids].each do |id|
        label = Label.find id
        labels << label
      end
      @food.labels = labels

      allergens = []
      @food_attr[:allergens_ids].each do |id|
        allergen = Allergen.find id
        allergens << allergen
      end
      @food.allergens = allergens
    end

    def set_images
      images = []
      @food_attr[:images_ids].each do |id|
        image = Image.find id
        images << image
      end
      raise_gql_error code: 'images_empty' if images.empty?
      @food.images = images
    end
  end
end

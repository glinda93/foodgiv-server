# frozen_string_literal: true

module Mutations
  class SaveProfile < Mutations::BaseMutation
    include AuthHelper

    graphql_name 'save_profile'

    argument :profile, Types::SaveProfileInput, required: true

    field :user, Types::UserType, null: false

    def resolve(input)
      auth_check
      @user = context[:current_user]
      @profile = input[:profile]
      set_base_info
      set_image
      @user.save!
      { user: @user }
    end

    def set_base_info
      @user[:summary] = @profile[:summary]
      @user[:favorite_food] = @profile[:favorite_food]
      @user[:hated_food] = @profile[:hated_food]
      if @profile[:location]
        if @user.location
          @user.location.lat = @profile[:location][:lat]
          @user.location.lng = @profile[:location][:lng]
          @user.location.place_id = @profile[:location][:place_id]
          @user.location.formatted_address = @profile[:location][:formatted_address]
          @user.location.save!
        else
          @user.location = Location.new({
                                          lat: @profile[:location][:lat],
                                          lng: @profile[:location][:lng],
                                          place_id: @profile[:location][:place_id],
                                          formatted_address: @profile[:location][:formatted_address]
                                        })
        end
      end
    end

    def set_image
      return unless @profile[:image_id]

      image = Image.find @profile[:image_id]
      @user.profile_image = image
    end
  end
end

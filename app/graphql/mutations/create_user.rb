# frozen_string_literal: true

module Mutations
  class CreateUser < Mutations::BaseMutation
    include SocialLoginHelper
    include GraphqlHelper

    graphql_name 'create_user'
    description 'Sign up'
    argument :credentials, Types::AuthProviderSignupInput, required: true

    field :user, Types::UserType, null: true
    field :errors, [Types::ErrorType], null: true

    def resolve(input)
      credentials = input[:credentials]
      error = validate credentials
      return handle_error(error) if error

      @user = create_user credentials
      {
        user: @user,
        error: nil
      }
    end

    def validate(credentials)
      check_main_credential credentials
      check_duplication credentials
    end

    def check_main_credential(credentials)
      if credentials[:provider] == 'EMAIL' && credentials[:email].blank?
        raise_gql_error code: 'email_empty'
      end
      if credentials[:provider] != 'EMAIL' && credentials[:access_token].blank?
        raise_gql_error code: 'access_token_empty'
      end
    end

    def check_duplication(credentials)
      email = credentials[:email]
      user_id = credentials[:user_id]
      user = email ? User.find_by(email: credentials[:email]) : nil
      raise_gql_error code: 'email_duplicated' if user

      provider_user_id = credentials[:provider_user_id]
      user = case credentials[:provider]
             when 'GOOGLE'
               user_id ? User.find_by(google_user_id: provider_user_id) : nil
             when 'FACEBOOK'
               user_id ? User.find_by(facebook_user_id: provider_user_id) : nil
             end
      raise_gql_error code: 'auth_provider_id_duplicated' if user
    end

    def create_user(credentials)
      user_data = {
        first_name: credentials[:first_name],
        last_name: credentials[:last_name],
        email: credentials[:email],
        password: credentials[:password],
        created_at: DateTime.now
      }
      if credentials[:provider_user_id]
        case credentials[:provider]
        when 'GOOGLE'
          user_data[:google_user_id] = credentials[:provider_user_id]
        when 'FACEBOOK'
          user_data[:facebook_user_id] = credentials[:provider_user_id]
        end
      end
      begin
        User.create!(user_data)
      rescue Exception => e
        raise GraphQL::ExecutionError, e
      end
    end
  end
end

# frozen_string_literal: true

module Mutations
  class CompleteRequest < Mutations::BaseMutation
    include AuthHelper
    include GraphqlHelper
    graphql_name 'complete_request'
    description 'Giver completes a request'
    argument :request_id, Integer, required: true

    field :request, Types::RequestType, null: false

    def resolve(input)
      @input = input
      auth_check
      check_request
      save_request
      notify_to_receiver
      { request: @request }
    end

    def check_request
      @user = context[:current_user]
      @request = Request.where(id: @input[:request_id], giver_id: @user.id).first
      raise_gql_error code: 'no_such_request' unless @request
      raise_gql_error code: 'request_not_accepted' unless @request.status == 'ACCEPTED'
    end

    def save_request
      @request.completed_at = DateTime.now
      @request.status = 'COMPLETED'
      @request.save!
    end

    def notify_to_receiver; end
  end
end

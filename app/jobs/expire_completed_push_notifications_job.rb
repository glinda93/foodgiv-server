# frozen_string_literal: true

class ExpireCompletedPushNotificationsJob < ApplicationJob
  queue_as :default

  def perform(*_args)
    redis = Rails.cache.redis
    redis.keys('rpush:notifications:*').each do |key|
      next unless redis.ttl(key) == -1
      next unless redis.type(key) == 'hash'
      next if redis.hget(key, 'delivered').nil?

      redis.expire(key, 2.hours.to_i)
    end
  end
end

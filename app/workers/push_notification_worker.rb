# frozen_string_literal: true

class PushNotificationWorker
  include Sidekiq::Worker

  def perform(*_args)
    PushNotificationJob.perform_now
  end
end

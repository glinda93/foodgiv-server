# frozen_string_literal: true

require 'rails_helper'
require 'sidekiq/testing'
RSpec.describe ExpireCompletedPushNotificationsWorker, type: :worker do
  before do
    Sidekiq::Worker.clear_all
  end

  it 'expires redis keys for completed push notifications' do
    described_class.perform_async
    expect(described_class.jobs.size).to eq 1
  end
end

# frozen_string_literal: true

FactoryBot.define do
  factory :location do
    place_id { Faker::Alphanumeric.unique.alphanumeric(number: 16) }
    lat { Faker::Address.latitude }
    lng { Faker::Address.longitude }
    formatted_address { Faker::Address.full_address }

    trait :new_york do
      place_id { 'ChIJOwg_06VPwokRYv534QaPC8g' }
      lat { 40.7128 }
      lng { -74.006 }
      formatted_address { 'New York, NY, USA' }
    end

    trait :brooklyn do
      place_id { 'ChIJCSF8lBZEwokRhngABHRcdoI' }
      lat { 40.6782 }
      lng { -73.9442 }
      formatted_address { 'Brooklyn, NY, USA' }
    end

    trait :toronto do
      place_id { 'ChIJCSF8lBZEwokRhngABHRcdoI' }
      lat { 43.6532 }
      lng { -79.3832 }
      formatted_address { 'Toronto, ON, Canada' }
    end

    trait :longtan do
      place_id { 'ChIJB8b9vcQKSF4RiJpv0Pf7UPc' }
      lat { 43.9108 }
      lng { 126.562 }
      formatted_address { 'Longtan District, Jilin City, China' }
    end
  end
end

# frozen_string_literal: true

require 'faker'
require 'jwt'

Faker::Config.locale = 'en-US'

module SpecMainHelper
  def build_mock_auth_header(token)
    {
      'Authorization' => "Bearer #{token}"
    }
  end

  def get_jwt_from(user)
    payload = {
      data: user.id,
      exp: Time.now.to_i + 10_000
    }
    jwt_sign_without_lib payload
  end

  def get_auth_header_from(user)
    build_mock_auth_header get_jwt_from(user)
  end

  def jwt_sign_without_lib(payload)
    JWT.encode payload, ENV['JWT_SECRET_KEY'], ENV['JWT_ALGORITHM']
  end

  def shrine_image_data(filename = 'test.png', dir = 'app/assets/images/test')
    attacher = Shrine::Attacher.new
    uploaded_file = uploaded_image(filename, dir)
    attacher.set(uploaded_file)
    attacher.set_derivatives(
      large: uploaded_file,
      medium: uploaded_file,
      small: uploaded_file
    )
    attacher.column_data
  end

  def uploaded_image(filename, dir)
    file = File.open("#{dir}/#{filename}", binmode: true)
    uploaded_file = ImageUploader.upload(file, :store)
    uploaded_file
  end
end

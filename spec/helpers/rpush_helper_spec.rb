# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RpushHelper, type: :helper do
  describe '#uncached_notification_subscribed_users' do
    it 'returns users that subscribed to notification' do
      user = create(:user, :notification_subscribed)
      expect(helper.uncached_notification_subscribed_users(user.notification_frequency).first).to eq user
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'

def build_food_query_param(param = nil)
  return '' unless param
  return '' if param[:filter].nil? && param[:sort].nil?

  <<~PARAM
      (
        #{if param[:filter].nil?
            ''
          else
            "
          filter: {
            #{if param[:filter][:food_type].nil?
                ''
              else
                "
                foodType: #{param[:filter][:food_type]}
                "
            end}
            #{if param[:filter][:labels_ids].nil?
                ''
              else
                "
                labelsIds: #{param[:filter][:labels_ids]}
                "
            end}
            #{if param[:filter][:allergens_ids].nil?
                ''
              else
                "
                allergensIds: #{param[:filter][:allergens_ids]}
                "
            end}
            #{if param[:filter][:distance].nil?
                ''
              else
                "
                distance: #{param[:filter][:distance]}
                "
            end}
            #{if param[:filter][:address].nil?
                ''
              else
                "
                address: \"#{param[:filter][:address]}\"
                "
            end}
            #{if param[:filter][:geolocation].nil?
                ''
              else
                "
                geolocation: {
                  lat: #{param[:filter][:geolocation][:lat]}
                  lng: #{param[:filter][:geolocation][:lng]}
                }
                "
            end}
            #{if param[:filter][:keyword].nil?
                ''
              else
                "
                keyword: \"#{param[:filter][:keyword]}\"
                "
            end}
          }
        "
      end}
      #{if param[:sort].nil?
          ''
        else
          "
          sort: #{param[:sort]}
          "
      end}
    )
  PARAM
end

def foods_query(param = nil)
  <<~GQL
    query {
      foodsConnection #{build_food_query_param(param)} {
        pageInfo {
          hasNextPage
          hasPreviousPage
          startCursor
          endCursor
        }
        totalCount
        nodes {
          id
          title
          description
          user {
            id
            location {
              lat
              lng
            }
            firstName
            lastName
          }
          location {
            placeId
            lat
            lng
            formattedAddress
          }
          images {
            image {
              id
              url
            }
            large {
              url
            }
          }
          pickupTime
          expDate
          labels {
            name
          }
          allergens {
            name
          }
          requestCount
        }
      }
    }
  GQL
end

def food_with_labels(labels, food = create(:food))
  food.labels = labels
  food.save!
  food
end

def food_with_allergens(allergens, food = create(:food))
  food.allergens = allergens
  food.save!
  food
end

def food_with(labels = [], allergens = [], food = create(:food))
  food_with_labels(labels, food_with_allergens(allergens, food))
end

module Types
  RSpec.describe QueryType, type: :request do
    describe '#foodsConnection' do
      context 'when there are no foods in the database' do
        it 'returns no nodes' do
          post '/graphql', params: { query: foods_query }
          json = JSON.parse response.body
          expect(json['data']['foodsConnection']['totalCount']).to eq(0)
        end
      end

      context 'when there are foods in the database' do
        it 'returns foods' do
          foods = 3.times.map { |_i| create(:food) }
          post '/graphql', params: { query: foods_query }
          json = JSON.parse response.body
          expect(json['data']['foodsConnection']['totalCount']).to eq(3)
          actual = foods.last
          expected = json['data']['foodsConnection']['nodes'].first
          expect(expected['id']).to eq(actual.id.to_s)
        end

        it 'does not show deleted/unlisted/accepted/expired/completed foods' do
          food_deleted = create(:food, :deleted)
          food_unlisted = create(:food, :unlisted)
          food_expired = create(:food, :expired)
          food_accepted = create(:food, :accepted)
          food_completed = create(:food, :completed)
          post '/graphql', params: { query: foods_query }
          json = JSON.parse response.body
          expect(json['data']['foodsConnection']['totalCount']).to eq(0)
          expect(json['data']['foodsConnection']['nodes'].size).to eq(0)
        end
      end

      context 'when filter param is given' do
        it 'is able to filter food type' do
          food_prepared = create(:food, :prepared)
          food_unprepared = create(:food, :unprepared)
          post '/graphql', params: { query: foods_query({ filter: { food_type: 'PREPARED' } }) }
          json = JSON.parse response.body
          expect(json['data']['foodsConnection']['nodes'].size).to eq(1)
          expect(json['data']['foodsConnection']['nodes'][0]['id']).to eq(food_prepared.id.to_s)
          create_list(:food, 2, :prepared)
          create_list(:food, 3, :unprepared)
          post '/graphql', params: { query: foods_query({ filter: { food_type: 'UNPREPARED' } }) }
          json = JSON.parse response.body
          expect(json['data']['foodsConnection']['totalCount']).to eq(4)
        end

        it 'is able to filter labels' do
          label_vegan = create(:label, name: 'vegan')
          label_raw = create(:label, name: 'raw')
          label_packaged = create(:label, name: 'packaged')
          label_greasy = create(:label, name: 'greasy')
          label_gluten = create(:label, name: 'gluten')
          food_vegan_raw = food_with_labels [label_vegan, label_raw]
          food_vegan_packaged = food_with_labels [label_vegan, label_packaged]
          food_raw_greasy = food_with_labels [label_raw, label_greasy]
          food_packaged_greasy_gluten = food_with_labels([label_packaged, label_greasy, label_gluten])
          post '/graphql', params: { query: foods_query({ filter: { labels_ids: [label_vegan.id] } }) }
          json = JSON.parse response.body
          expect(json['data']['foodsConnection']['totalCount']).to eq(2)
          post '/graphql', params: { query: foods_query({ filter: { labels_ids: [label_gluten.id, label_raw.id] } }) }
          json = JSON.parse response.body
          expect(json['data']['foodsConnection']['totalCount']).to eq(3)
        end

        it 'is able to filter allergens' do
          allergen_milk = create(:allergen, name: 'milk')
          allergen_fish = create(:allergen, name: 'fish')
          food_milk = food_with_allergens [allergen_milk]
          food_fish = food_with_allergens [allergen_fish]
          food_milk_fish = food_with_allergens [allergen_milk, allergen_fish]
          food_none = food_with_allergens []
          post '/graphql', params: { query: foods_query({ filter: { allergens_ids: [allergen_milk.id, allergen_fish.id] } }) }
          json = JSON.parse response.body
          expect(json['data']['foodsConnection']['totalCount']).to eq(1)
        end

        it 'is able to filter all params' do
          label_vegan = create(:label, name: 'vegan')
          label_raw = create(:label, name: 'raw')
          label_packaged = create(:label, name: 'packaged')
          label_greasy = create(:label, name: 'greasy')
          allergen_milk = create(:allergen, name: 'milk')
          allergen_fish = create(:allergen, name: 'fish')
          milk_fish_curry = food_with([label_packaged, label_greasy], [allergen_milk, allergen_fish], create(:food, :prepared, title: 'Milk fish curry'))
          cow_milk = food_with([label_raw], [allergen_milk], create(:food, :unprepared, title: "Cow's Milk"))
          frozen_sardine = food_with([label_packaged], [allergen_fish], create(:food, :unprepared, title: 'Frozen Sardine'))
          strawberry_oatmeal = food_with([label_raw, label_vegan], [], create(:food, :prepared, title: 'Strawberry Oatmeal'))

          # milk_fish_curry
          post '/graphql', params: { query: foods_query({ filter: { food_type: 'PREPARED', labels_ids: [label_greasy.id] } }) }
          json = JSON.parse response.body
          expect(json['data']['foodsConnection']['totalCount']).to eq(1)

          # cow_milk and strawberry_oatmeal
          post '/graphql', params: { query: foods_query({ filter: { labels_ids: [label_raw.id], allergens_ids: [allergen_fish.id] } }) }
          json = JSON.parse response.body
          expect(json['data']['foodsConnection']['totalCount']).to eq(2)

          # cow_milk
          post '/graphql', params: { query: foods_query({ filter: { food_type: 'UNPREPARED', labels_ids: [label_raw.id], allergens_ids: [allergen_fish.id] } }) }
          json = JSON.parse response.body
          expect(json['data']['foodsConnection']['totalCount']).to eq(1)
          expect(json['data']['foodsConnection']['nodes'][0]['id']).to eq(cow_milk.id.to_s)
        end

        it 'sorts by created_at desc by default' do
          food_1 = create(:food)
          food_2 = create(:food)
          food_3 = create(:food)
          food_1.created_at = DateTime.now + 1
          food_1.save!
          post '/graphql', params: { query: foods_query }
          json = JSON.parse response.body
          expect(json['data']['foodsConnection']['nodes'][0]['id']).to eq(food_1.id.to_s)
          expect(json['data']['foodsConnection']['nodes'][1]['id']).to eq(food_3.id.to_s)
        end

        it 'is able to sort by distance' do
          food_toronto = create(:food, location: create(:location, :toronto))
          food_ny = create(:food, location: create(:location, :new_york))
          food_brooklyn = create(:food, location: create(:location, :brooklyn))
          food_longtan = create(:food, location: create(:location, :longtan))
          new_yorker = create(:user, location: create(:location, :new_york))
          post '/graphql', params: { query: foods_query(sort: 'CLOSEST') }, headers: get_auth_header_from(new_yorker)
          json = JSON.parse response.body
          expect(json['data']['foodsConnection']['nodes'][0]['id']).to eq(food_ny.id.to_s)
          expect(json['data']['foodsConnection']['nodes'][1]['id']).to eq(food_brooklyn.id.to_s)
          expect(json['data']['foodsConnection']['nodes'][2]['id']).to eq(food_toronto.id.to_s)
          expect(json['data']['foodsConnection']['nodes'][3]['id']).to eq(food_longtan.id.to_s)
        end

        it 'is able to filter by distance and address' do
          puts 'Warning: filter by distance and address consumes Google Geocode API'
          food_toronto = create(:food, location: create(:location, :toronto))
          food_ny = create(:food, location: create(:location, :new_york))
          food_brooklyn = create(:food, location: create(:location, :brooklyn))
          food_longtan = create(:food, location: create(:location, :longtan))
          new_yorker = create(:user, location: create(:location, :new_york))
          post '/graphql', params: { query: foods_query(
            filter: {
              address: 'New York',
              distance: 10
            },
            sort: 'CLOSEST'
          ) }, headers: get_auth_header_from(new_yorker)
          json = JSON.parse response.body
          connection = json['data']['foodsConnection']
          expect(connection['totalCount']).to eq 2
          expect(connection['nodes'][0]['id']).to eq food_ny.id.to_s
          expect(connection['nodes'][1]['id']).to eq food_brooklyn.id.to_s
          post '/graphql',
               params: {
                 query: foods_query(
                   filter: {
                     address: 'toronto, canada',
                     distance: 10
                   },
                   sort: 'CLOSEST'
                 )
               },
               headers: get_auth_header_from(new_yorker)
          json = JSON.parse response.body
          connection = json['data']['foodsConnection']
          expect(connection['totalCount']).to eq 1
          expect(connection['nodes'][0]['id']).to eq food_toronto.id.to_s
        end

        it 'is able to filter by geolocation and distance' do
          puts 'Warning: filter by geolocation and distance consumes Google Geocode API'
          food_toronto = create(:food, location: create(:location, :toronto))
          food_ny = create(:food, location: create(:location, :new_york))
          food_brooklyn = create(:food, location: create(:location, :brooklyn))
          food_longtan = create(:food, location: create(:location, :longtan))
          new_yorker = create(:user, location: create(:location, :new_york))
          post '/graphql', params: { query: foods_query(
            filter: {
              geolocation: {
                lat: 40.7128,
                lng: -73.9442
              },
              distance: 10
            },
            sort: 'CLOSEST'
          ) }, headers: get_auth_header_from(new_yorker)
          json = JSON.parse response.body
          connection = json['data']['foodsConnection']
          expect(connection['totalCount']).to eq 2
          expect(connection['nodes'][0]['id']).to eq food_ny.id.to_s
          expect(connection['nodes'][1]['id']).to eq food_brooklyn.id.to_s
        end

        it 'is able to filter by keyword' do
          food_toronto = create(:food, location: create(:location, :toronto))
          food_ny = create(:food, location: create(:location, :new_york))
          food_brooklyn = create(:food, location: create(:location, :brooklyn))
          food_longtan = create(:food, location: create(:location, :longtan))
          post '/graphql',
               params: {
                 query: foods_query(
                   filter: {
                     keyword: 'longtan'
                   }
                 )
               }
          json = JSON.parse response.body
          connection = json['data']['foodsConnection']
          expect(connection['totalCount']).to eq 1
          expect(connection['nodes'][0]['id']).to eq food_longtan.id.to_s
        end

        it 'passes final trial' do
          # user
          new_yorker = create(:user, location: create(:location, :new_york))
          # locations
          new_york = create(:location, :new_york)
          brooklyn = create(:location, :brooklyn)
          toronto = create(:location, :toronto)
          # labels & allergens
          label_meat = create(:label, name: 'Meat')
          label_fruit = create(:label, name: 'Fruit')
          label_veg = create(:label, name: 'Vegetable')
          label_fish = create(:label, name: 'Fish')
          allergen_brocolli = create(:allergen, name: 'Broccoli')
          allergen_sardine = create(:allergen, name: 'Sardine')
          allergen_peanut = create(:allergen, name: 'Peanut')
          # foods
          sardines_beccafico = food_with(
            [label_meat, label_fruit, label_fish],
            [allergen_sardine],
            create(:food, :prepared, title: 'Sardines Beccafico', location: toronto)
          )
          broccoli_peanut_salad = food_with(
            [label_fruit, label_veg],
            [allergen_brocolli, allergen_peanut],
            create(:food, :prepared, title: 'Broccoli Peanut Salad', location: brooklyn)
          )
          strawberry = food_with(
            [label_fruit],
            [],
            create(:food, :unprepared, title: 'Strawberry 10 lb', location: toronto)
          )
          frozen_mingtai = food_with(
            [label_fish],
            [],
            create(:food, :unprepared, title: 'Frozen Mingtai', location: new_york)
          )
          tomato = food_with(
            [label_fruit, label_veg],
            [],
            create(:food, :unprepared, title: 'Tomato', location: brooklyn)
          )
          # sardines_beccafico and broccoli_peanut_salad
          post '/graphql', params: {
            query: foods_query({
                                 filter: {
                                   food_type: 'PREPARED',
                                   labels_ids: [label_fruit.id],
                                   allergens_ids: []
                                 },
                                 sort: 'CLOSEST'
                               })
          }
          json = JSON.parse response.body
          expect(json['data']['foodsConnection']['nodes'].length).to eq(2)

          #  strawberry and tomato
          post '/graphql', params: {
            query: foods_query({
                                 filter: {
                                   food_type: 'UNPREPARED',
                                   labels_ids: [label_fruit.id],
                                   allergens_ids: [allergen_brocolli.id, allergen_sardine.id]
                                 },
                                 sort: 'CLOSEST'
                               })
          }
          json = JSON.parse response.body
          expect(json['data']['foodsConnection']['nodes'].length).to eq(2)
          expect(json['data']['foodsConnection']['nodes'][0]['id']).to eq(tomato.id.to_s)
        end
      end
    end
  end
end

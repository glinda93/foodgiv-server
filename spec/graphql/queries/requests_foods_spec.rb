# frozen_string_literal: true

require 'rails_helper'
require 'spec_helper'

def build_requests_foods_connection_param(status)
  return '' if status.blank?

  <<~PARAM
    (
      filter: {
        status: #{status}
      }
    )
  PARAM
end

def requests_foods_connection_query(status = nil)
  <<~GQL
    query {
      requestsFoodsConnection #{build_requests_foods_connection_param status} {
        pageInfo {
          hasNextPage
          hasPreviousPage
          startCursor
          endCursor
        }
        totalCount
        nodes {
          id
        }
      }
    }
  GQL
end

module Types
  RSpec.describe QueryType, type: :request do
    let(:user) { create(:user) }

    it 'returns foods that has requests belong to user' do
      food_1 = create(:food, user: user)
      food_2 = create(:food, user: user)
      food_3 = create(:food)
      food_4 = create(:food)
      request_1 = create(:request, food: food_2)
      request_2 = create(:request, food: food_3, receiver: user)
      request_3 = create(:request, food: food_4)
      post '/graphql', params: { query: requests_foods_connection_query }, headers: get_auth_header_from(user)
      json = JSON.parse response.body
      expect(json['data']['requestsFoodsConnection']['totalCount']).to eq 2
      expect(json['data']['requestsFoodsConnection']['nodes'].collect { |n| n['id'] }).to include(
        food_2.id.to_s,
        food_3.id.to_s
      )
    end

    it 'filters by food status' do
      food_1 = create(:food, user: user)
      food_2 = create(:food, user: user)
      food_3 = create(:food, :completed)
      food_4 = create(:food)
      request_1 = create(:request, food: food_2)
      request_2 = create(:request, food: food_3, receiver: user)
      request_3 = create(:request, :completed, food: food_4)
      post '/graphql', params: { query: requests_foods_connection_query('COMPLETED') }, headers: get_auth_header_from(user)
      json = JSON.parse response.body
      expect(json['data']['requestsFoodsConnection']['totalCount']).to eq 1
      expect(json['data']['requestsFoodsConnection']['nodes'].collect { |n| n['id'] }).to include(
        food_3.id.to_s
      )
    end
  end
end

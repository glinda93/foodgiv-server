# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def user_query(id)
  <<~GQL
    query {
      user(id: #{id}) {
        id
        firstName
        lastName
        phoneVerified
        summary
        favoriteFood
        hatedFood
        location {
          placeId
          lat
          lng
          formattedAddress
        }
        distance
        giverReviewCount
        giverRating
        receiverReviewCount
        receiverRating
        createdAt
      }
    }
  GQL
end

module Types
  RSpec.describe QueryType, type: :request do
    describe '#user' do
      it 'returns profile of other user' do
        viewer = create(:user)
        viewee = create(:user)
        request = create(:request, giver: viewee)
        review = create(:review, :for_giver, request: request, reviewer: request.receiver)
        post '/graphql', params: { query: user_query(viewee.id) }, headers: get_auth_header_from(viewer)
        json = JSON.parse response.body
        profile = json['data']['user']
        %w[
          id
          firstName
          lastName
          phoneVerified
          summary
          favoriteFood
          hatedFood
          location
          distance
          giverReviewCount
          giverRating
          receiverReviewCount
          receiverRating
          createdAt
        ].each do |key|
          expect(profile[key]).not_to be_nil
        end
      end

      context 'when user location is not set' do
        it 'returns nil for distance field' do
          user_1 = create(:user, location: nil)
          user_2 = create(:user)
          post '/graphql', params: { query: user_query(user_2.id) }, headers: get_auth_header_from(user_1)
          json = JSON.parse response.body
          profile = json['data']['user']
          expect(profile['distance']).to be_nil
          post '/graphql', params: { query: user_query(user_1.id) }, headers: get_auth_header_from(user_2)
          json = JSON.parse response.body
          profile = json['data']['user']
          expect(profile['distance']).to be_nil
        end
      end
    end
  end
end

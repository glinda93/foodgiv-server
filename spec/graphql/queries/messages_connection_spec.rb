# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def build_message_connection_query_param(request_id = nil, show_archived = nil)
  return '' if request_id.nil? && show_archived.nil?

  <<~PARAM
    (
      filter: {
        #{if request_id.nil?
            ''
          else
            "
          requestId: #{request_id}
          "
        end}
        #{if show_archived.nil?
            ''
          else
            "
          showArchived: #{show_archived}
          "
        end}
      }
    )
  PARAM
end

def messages_connection_query(request_id = nil, show_archived = nil)
  <<~GQL
    query {
      messagesConnection #{build_message_connection_query_param(request_id, show_archived)} {
        pageInfo {
          hasNextPage
          hasPreviousPage
          startCursor
          endCursor
        }
        totalCount
        nodes {
          id
          sender {
            id
          }
          receiver {
            id
          }
          request {
            id
          }
          food {
            id
          }
          content
          contentType
          readAt
          senderArchived
          receiverArchived
          createdAt
          updatedAt
        }
      }
    }
  GQL
end

module Types
  RSpec.describe QueryType, type: :request do
    describe '#messages_connection' do
      context 'when jwt token is invalid' do
        it 'throws authentication exception' do
          post '/graphql', params: { query: messages_connection_query(1) }, headers: build_mock_auth_header('token_that_never_works')
          json = JSON.parse response.body
          expect(json['data']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'AUTHENTICATION_FAILED'
        end
      end

      it 'returns messages' do
        message_1 = create(:message)
        message_2 = create(:message, request: message_1.request, sender: message_1.sender, receiver: message_1.receiver)
        message_3 = create(:message, request: message_1.request, sender: message_1.sender, receiver: message_1.receiver)
        post '/graphql', params: { query: messages_connection_query(message_1.request.id) }, headers: get_auth_header_from(message_1.sender)
        json = JSON.parse response.body
        expect(json['data']['messagesConnection']['totalCount']).to eq 3
        expect(json['data']['messagesConnection']['nodes'].count).to eq 3
      end

      it 'does not return archived messages by default' do
        message_1 = create(:message)
        message_2 = create(:message, request: message_1.request, sender: message_1.sender, receiver: message_1.receiver, sender_archived: true)
        message_3 = create(:message, request: message_1.request, sender: message_1.sender, receiver: message_1.receiver)
        post '/graphql', params: { query: messages_connection_query(message_1.request.id) }, headers: get_auth_header_from(message_1.sender)
        json = JSON.parse response.body
        expect(json['data']['messagesConnection']['totalCount']).to eq 2
        expect(json['data']['messagesConnection']['nodes'].count).to eq 2
      end

      it 'returns all messages including archived ones if show_archive is set true' do
        message_1 = create(:message)
        message_2 = create(:message, request: message_1.request, sender: message_1.sender, receiver: message_1.receiver, sender_archived: true)
        message_3 = create(:message, request: message_1.request, sender: message_1.sender, receiver: message_1.receiver)
        post '/graphql', params: { query: messages_connection_query(message_1.request.id, true) }, headers: get_auth_header_from(message_1.sender)
        json = JSON.parse response.body
        expect(json['data']['messagesConnection']['totalCount']).to eq 3
        expect(json['data']['messagesConnection']['nodes'].count).to eq 3
      end

      it 'is able to filter by request id' do
        message_1 = create(:message)
        message_2 = create(:message, request: message_1.request, sender: message_1.sender, receiver: message_1.receiver, sender_archived: true)
        message_3 = create(:message, sender: message_1.sender, receiver: message_1.receiver)
        post '/graphql', params: { query: messages_connection_query(message_1.request.id, false) }, headers: get_auth_header_from(message_1.sender)
        json = JSON.parse response.body
        expect(json['data']['messagesConnection']['totalCount']).to eq 1
      end

      it 'returns messages by created_at desc' do
        message_1 = create(:message, created_at: DateTime.now - 2.seconds)
        message_2 = create(:message, request: message_1.request, sender: message_1.sender, created_at: DateTime.now - 1.second)
        message_3 = create(:message, request: message_1.request, sender: message_1.sender, created_at: DateTime.now - 3.seconds)
        post '/graphql', params: { query: messages_connection_query(message_1.request.id) }, headers: get_auth_header_from(message_1.sender)
        json = JSON.parse response.body
        expect(json['data']['messagesConnection']['nodes'][0]['id']).to eq message_2.id.to_s
      end
    end
  end
end

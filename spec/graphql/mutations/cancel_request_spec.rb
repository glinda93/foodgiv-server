# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def cancel_request_mutation(request_id, relist = nil)
  <<~GQL
    mutation {
      cancelRequest(
        input: {
          requestId: #{request_id}
          #{if relist.nil?
              ''
            else
              "
          relist: #{relist}
          "
            end}
        }
      ) 
      {
        request {
          id
          message
          giver {
            id
          }
          receiver {
            id
          }
          food {
            id
          }
          acceptedAt
          declinedAt
          cancelledAt
          completedAt
          read
          status
          createdAt
        }
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    describe '#cancel_request' do
      context 'when jwt token is invalid' do
        it 'throws authentication exception' do
          post '/graphql', params: { query: cancel_request_mutation(1) }, headers: build_mock_auth_header('token_that_never_works')
          json = JSON.parse response.body
          expect(json['data']['cancelRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'AUTHENTICATION_FAILED'
        end
      end

      context 'when request is not found' do
        it 'returns code no_such_request' do
          post '/graphql', params: { query: cancel_request_mutation(1) }, headers: get_auth_header_from(create(:user))
          json = JSON.parse response.body
          expect(json['data']['cancelRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'no_such_request'
        end
      end

      context 'when receiver cancels a request' do
        context 'when request is already handled' do
          it 'returns already handled code' do
            # accepted request
            request = create(:request, :accepted)
            post '/graphql', params: { query: cancel_request_mutation(request.id) }, headers: get_auth_header_from(request.receiver)
            json = JSON.parse response.body
            expect(json['data']['cancelRequest']).to be_nil
            expect(json['errors'][0]['extensions']['code']).to eq 'request_already_accepted'
            # declined request
            request = create(:request, :declined)
            post '/graphql', params: { query: cancel_request_mutation(request.id) }, headers: get_auth_header_from(request.receiver)
            json = JSON.parse response.body
            expect(json['data']['cancelRequest']).to be_nil
            expect(json['errors'][0]['extensions']['code']).to eq 'request_already_declined'
            # cancelled request
            request = create(:request, :cancelled)
            post '/graphql', params: { query: cancel_request_mutation(request.id) }, headers: get_auth_header_from(request.receiver)
            json = JSON.parse response.body
            expect(json['data']['cancelRequest']).to be_nil
            expect(json['errors'][0]['extensions']['code']).to eq 'request_already_cancelled'
          end
        end

        context 'when everything is ok' do
          it 'changes request status to be CANCELLED' do
            request = create(:request)
            post '/graphql', params: { query: cancel_request_mutation(request.id) }, headers: get_auth_header_from(request.receiver)
            json = JSON.parse response.body
            request = json['data']['cancelRequest']['request']
            expect(request['cancelledAt']).to be_present
            expect(request['status']).to eq 'CANCELLED'
          end
        end
      end

      context 'when giver cancels a request' do
        context 'when request is not accepted' do
          it 'returns code request_not_accepted' do
            # new request
            request = create(:request)
            post '/graphql', params: { query: cancel_request_mutation(request.id) }, headers: get_auth_header_from(request.giver)
            json = JSON.parse response.body
            expect(json['data']['cancelRequest']).to be_nil
            expect(json['errors'][0]['extensions']['code']).to eq 'request_not_accepted'
            # declined request
            request = create(:request, :declined)
            post '/graphql', params: { query: cancel_request_mutation(request.id) }, headers: get_auth_header_from(request.giver)
            json = JSON.parse response.body
            expect(json['data']['cancelRequest']).to be_nil
            expect(json['errors'][0]['extensions']['code']).to eq 'request_not_accepted'
            # cancelled request
            request = create(:request, :cancelled)
            post '/graphql', params: { query: cancel_request_mutation(request.id) }, headers: get_auth_header_from(request.giver)
            json = JSON.parse response.body
            expect(json['data']['cancelRequest']).to be_nil
            expect(json['errors'][0]['extensions']['code']).to eq 'request_not_accepted'
          end
        end

        context 'when everything is ok' do
          it 'changes request status to be NEW' do
            request = create(:request, :accepted)
            post '/graphql', params: { query: cancel_request_mutation(request.id) }, headers: get_auth_header_from(request.giver)
            json = JSON.parse response.body
            request = json['data']['cancelRequest']['request']
            expect(request['status']).to eq 'NEW'
          end

          context 'when giver chooses to relist food' do
            it 'reverts food status to be LISTED' do
              request = create(:request, :accepted)
              post '/graphql', params: { query: cancel_request_mutation(request.id, true) }, headers: get_auth_header_from(request.giver)
              expect(Food.find(request.food.id).status).to eq 'LISTED'
            end
          end
        end
      end
    end
  end
end

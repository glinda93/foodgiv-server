# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def send_message_mutation(request_id, content, content_type = 'TEXT')
  <<~GQL
    mutation {
      sendMessage(
        input: {
          requestId: #{request_id},
          content: "#{content}"
          contentType: #{content_type}
        }
      ) {
        message {
          id
          content
          contentType
          readAt
          createdAt
          updatedAt
          request {
            id
          }
          food {
            id
          }
          sender {
            id
          }
          receiver {
            id
          }
        }
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    describe '#send_message' do
      context 'when jwt token is invalid' do
        it 'throws authentication exception' do
          post '/graphql', params: { query: send_message_mutation(1, 'foo') }, headers: build_mock_auth_header('token_that_never_works')
          json = JSON.parse response.body
          expect(json['data']['sendMessage']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'AUTHENTICATION_FAILED'
        end
      end

      context 'when request is not found' do
        it 'returns code no_such_request' do
          post '/graphql', params: { query: send_message_mutation(1, 'foo') }, headers: get_auth_header_from(create(:user))
          json = JSON.parse response.body
          expect(json['data']['sendMessage']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'no_such_request'
        end
      end

      context 'when food is not found' do
        it 'returns code no_such_food' do
          request = create(:request, food: create(:food, :deleted))
          post '/graphql', params: { query: send_message_mutation(request.id, 'foo') }, headers: get_auth_header_from(request.giver)
          json = JSON.parse response.body
          expect(json['data']['sendMessage']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'no_such_food'
        end
      end

      context 'when receiver is not available' do
        it 'returns code receiver_not_available' do
          request = create(:request, receiver: create(:user, :disabled))
          post '/graphql', params: { query: send_message_mutation(request.id, 'foo') }, headers: get_auth_header_from(request.giver)
          json = JSON.parse response.body
          expect(json['data']['sendMessage']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'receiver_not_available'
        end
      end

      context 'when food giver did not init chat' do
        it 'returns code no_food_giver_message' do
          request = create(:request)
          giver = request.giver
          receiver = request.receiver
          post '/graphql', params: { query: send_message_mutation(request.id, 'foo') }, headers: get_auth_header_from(receiver)
          json = JSON.parse response.body
          expect(json['data']['sendMessage']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'cannot_send_message_before_giver'
        end
      end

      context 'when everything is ok' do
        it 'creates a message' do
          request = create(:request)
          giver = request.giver
          receiver = request.receiver
          # giver says hi
          post '/graphql', params: { query: send_message_mutation(request.id, 'Hello, there') }, headers: get_auth_header_from(giver)
          json = JSON.parse response.body
          message = json['data']['sendMessage']['message']
          expect(message['id']).to be_present
          expect(message['content']).to eq 'Hello, there'
          expect(message['contentType']).to eq 'TEXT'
          expect(message['request']['id']).to eq request.id.to_s
          expect(message['food']['id']).to eq request.food.id.to_s
          expect(message['sender']['id']).to eq giver.id.to_s
          expect(message['receiver']['id']).to eq receiver.id.to_s
          # receiver says hi back
          post '/graphql', params: { query: send_message_mutation(request.id, 'Nice to meet you!') }, headers: get_auth_header_from(receiver)
          json = JSON.parse response.body
          message = json['data']['sendMessage']['message']
          expect(message['content']).to eq 'Nice to meet you!'
          expect(message['request']['id']).to eq request.id.to_s
          expect(message['food']['id']).to eq request.food.id.to_s
          expect(message['sender']['id']).to eq receiver.id.to_s
          expect(message['receiver']['id']).to eq giver.id.to_s
        end
      end
    end
  end
end

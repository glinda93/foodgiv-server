# frozen_string_literal: true

require 'rails_helper'

RSpec.describe FoodNotificationJob, type: :job do
  describe '#perform' do
    it 'notifies new foods to user' do
      food = create(:food)
      food_2 = create(:food, location: food.location)
      user = create(:user, :notification_subscribed, location: food.location)
      described_class.perform_now(frequency: user.notification_frequency)
    end
  end
end

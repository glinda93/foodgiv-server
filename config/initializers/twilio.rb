# frozen_string_literal: true

Twilio.configure do |config|
  config.account_sid = Rails.configuration.twilio.account_sid
  config.auth_token = Rails.configuration.twilio.auth_token
end

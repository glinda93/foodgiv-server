# frozen_string_literal: true

require 'sidekiq/web'
require 'sidekiq-scheduler/web'

Rails.application.routes.draw do
  mount GraphiQL::Rails::Engine, at: '/graphiql', graphql_path: '/graphql' if Rails.env.development?
  mount Sidekiq::Web => '/sidekiq'
  
  post '/graphql', to: 'graphql#execute'
  post '/images/new', to: 'image#new'
  post '/images/multiple-upload', to: 'image#multiple_upload'
end

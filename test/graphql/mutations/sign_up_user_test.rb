# frozen_string_literal: true

require 'test_helper'

module Mutations
  module Users
    class SignUpTest < ActiveSupport::TestCase
      def perform(args = {})
        Mutations::CreateUser.new(object: nil, field: nil, context: { session: {} }).resolve(args)
      end

      test 'should successfully create users' do
        result = perform(
          credentials: {
            provider: 'EMAIL',
            email: 'gmail@chucknorris.com',
            first_name: 'Chuck',
            last_name: 'Norris',
            password: '12345678'
          }
        )
        assert result[:user].present?
        assert result[:user][:id].present?
      end

      test 'should check email duplication' do
        result = perform(
          credentials: {
            provider: 'EMAIL',
            email: 'gmail@chucknorris.com',
            first_name: 'Chuck',
            last_name: 'Norris',
            password: '12345678'
          }
        )
        assert result[:user].present?
        assert result[:user][:id].present?

        assert_raise 'User with the same email address already exists' do
          perform(
            credentials: {
              provider: 'GOOGLE',
              access_token: '100000000000001',
              email: 'gmail@chucknorris.com',
              first_name: 'Chuck',
              last_name: 'Norris1',
              password: '1233333'
            }
          )
        end
      end

      test 'should check main credential presence' do
        assert_raises 'Email is required' do
          perform(
            credentials: {
              provider: 'EMAIL',
              first_name: 'Chuck',
              last_name: 'Norris1',
              password: '1233333'
            }
          )
        end
        assert_raises 'Auth provider access token is required' do
          perform(
            credentials: {
              provider: 'FACEBOOK',
              email: 'gmail@chucknorris.com',
              first_name: 'abc',
              password: '111111'
            }
          )
        end
      end

      test 'should do validation check' do
        assert_raises GraphQL::ExecutionError do
          perform(
            credentials: {
              provider: 'EMAIL',
              first_name: 'A' * 192,
              email: 'fffff'
            }
          )
        end
      end
    end
  end
end
